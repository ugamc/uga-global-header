# University of Georgia Global Header

![UGA header](./screenshot.png)

## What’s Included?

- `src` directory — uncompiled source code
- `dist` directory — compiled code ready for use in production
- `screenshot.png` — intended display of header
- build scripts and dependencies

## Installation Instructions

1. Upload contents of `dist/_assets` to your server. Make note of upload location if uploading to a different directory.
2. Add HTML code from `dist/index.html` to your page or template.
   - Contents of the `<body />` tag are the entire footer. Paste at the start of your HTML document `<body>`.
   - Include the stylesheet `<link />` tag within the `<head>…</head>`.
   - Include the JavaScript `<script />` tags at the end of the `<body>…</body>`
   - Update your paths to match your upload location.
3. Compare display of the header on your site to `screenshot.png`. If significant differences exist between the example and your site, adjust CSS accordingly.

### Dependencies

[jQuery](https://jquery.com/) is used to toggle the search input and the mobile menu. If you're including either of these features in your site, make sure jQuery is loaded before including `uga-footer.js`. Alternatively, you can write custom code to replicate this functionality.

### Optional Dependencies

#### Icons

If using icon links and/or the search button, icons are loaded through [Font Awesome 6](https://fontawesome.com/). Be sure to include the required JavaScript either locally or through a CDN.

**Example**

```
<script defer src="https://use.fontawesome.com/releases/v6.4.2/js/solid.js" integrity="sha384-6FXzJ8R8IC4v/SKPI8oOcRrUkJU8uvFK6YJ4eDY11bJQz4lRw5/wGthflEOX8hjL" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v6.4.2/js/brands.js" integrity="sha384-zJ8/qgGmKwL+kr/xmGA6s1oXK63ah5/1rHuILmZ44sO2Bbq1V3p3eRTkuGcivyhD" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v6.4.2/js/fontawesome.js" integrity="sha384-Qmms7kHsbqYnKkSwiePYzreT+ufFVSNBhfLOEp0sEEfEVdORDs/aEnGaJy/l4eoy" crossorigin="anonymous"></script>
```

## Using the Example Code

### Content

The links and content used in this code serve as examples only. The only required element is the University of Georgia wordmark (with or without the shield icon, depending on the logo used on your website) which links back to the [UGA homepage](https://www.uga.edu/).

### Link Formats

The three link formats presented (text, button, and icon) are options available for use. You can choose to use one or more styles, or remove the additional links completely. You can also reorder the link styles as desired. The number of links should be limited so that the style does not break at smaller screen sizes.

### Mobile Menu

The code includes a basic implementation of a mobile menu which collapses all links into a dropdown menu. If preferrable, these links can be moved to your own site mobile navigation rather than use two mobile menus.

### Styling

#### Background Color

The header can be displayed with a white, black, or red background. See the example code for instructions on how to select your background color.

Background color can be changed by adding the appropriate class to the main `ugaheader` element.

- Bulldog Red — `ugaheader--red`
- Arch Black — `ugaheader--black`
- Chapel Bell White — `ugaheader--white`

#### Border

The header can also include a colored bottom-border to add contrast between the UGA header and the rest of your site.

To use a border, add the class `ugaheader--border` to the main `ugaheader` element and add an additional class to specify the color:

- Bulldog Red — `ugaheader--border-red`
- Arch Black — `ugaheader--border-black`
- Chapel Bell White — `ugaheader--border-white`

#### Wordmark

The _University of Georgia_ wordmark links to the university homepage. Websites that display a UGA logo that includes the shield should use the basic wordmark without the shield to avoid duplication of the shield element.

The header will default to the text-only wordmark (no shield). To use the wordmark with the shield, add the class `ugaheader__wordmark--shield` to the `ugaheader__wordmark` element.

#### Fluid-width vs. Fixed-width

The header spans the entire width of the page by default. If you wish for the header to be fixed-width, add the class `ugaheader__container--max-widths` to the `ugaheader__container` element. You may need to adjust the CSS to match your site's responsive breakpoints.

## Developer Notes

The `dist` directory contains production-ready code. If you need to make modifications to the source code and create your own build, here are some instructions.

### Prerequisites

- [Node.js/NPM](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install/)
- [Gulp](http://gulpjs.com/)

### Installing

This application uses Node.js v10.15.0. It is recommended to use a tool like [Node Version Manager](https://github.com/nvm-sh/nvm) to manage multiple active node.js versions.

After installing prerequisites, install the app dependencies.

```
yarn install
```

Start up Gulp!

```
gulp
```

The task runner will build pages, CSS, and JS and start a server. The site will automatically open in a browser to preview.

### Deployment

The custom script command `yarn run build` will run Gulp with a flag produce new production-ready code

### Issues

Please report any issues or proposed modifications through Bitbucket’s **Issues** feature for this repository.
