(function($) {
    if ( $( "#ugaheader__search-toggle" ).length ) {
	    $('#ugaheader__search-toggle').on('click', function(e) {
			$this = $(this);
			if ( $this.attr('aria-expanded') === 'false' ) {
				$this.attr('aria-expanded', 'true');
				$('#ugaheader__search-input').focus();
			} else {
				$this.attr('aria-expanded', 'false');
			}
		});
    }
    if ( $( "#ugaheader__nav-menu-toggle" ).length ) {
	    $('#ugaheader__nav-menu-toggle').on('click', function(e) {
	    	$this = $(this);
	    	if ( $this.attr('aria-expanded') === 'false' ) {
				$this.attr('aria-expanded', 'true');
				$this.addClass('open');
			} else {
				$this.attr('aria-expanded', 'false');
				$this.removeClass('open');
			}
	    });
	}
})(jQuery);